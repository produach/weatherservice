# Installing and Managing the Windows Service
After building the application, the new Windows Service can be published using **dotnet publish**:

dotnet publish -c Release -o c:\weatherservice

To control Windows Services, the **sc** command can be used. Creating a new Windows Service is done using **sc create** passing the name of the service and the **binPath** parameter referencing the executable:

sc create “Weather Service” binPath=c:\weatherservice\weatherService.exe

The status of the service can be queried using the Services **MMC**, or with the command line **sc query**:

sc query “Weather Service”

After the service is created, it is stopped and need to be started:

sc start “Weather Service”

To stop and delete the service, the **sc stop** and **sc delete** commands can be used.