using System;
using System.IO;
using System.Text.Json;
using RestSharp;
using weatherservice.Model;

namespace weatherservice.Services
{
    public interface IWeatherApi
    {
        WeatherModel GetWeatherData();
    }

    public class WeatherApi : IWeatherApi
    {
        private readonly WorkerOptions _options;

        public WeatherApi(WorkerOptions options)
        {
            _options = options;
        }
        public WeatherModel GetWeatherData()
        {
            WeatherModel model;
            try
            {
                var apiKey = _options.ApiKey;
                var city = _options.City;
                var client = new RestClient($"http://api.weatherapi.com/v1/current.json?key={apiKey}&q={city}");
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);

                model = JsonSerializer.Deserialize<WeatherModel>(response.Content.ToString());
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Somthing wrong happend:", ex);
            }

            return model;
        }
    }
}