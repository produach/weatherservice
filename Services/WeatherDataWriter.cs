using System;
using System.IO;
using weatherservice.Model;

namespace weatherservice.Services
{
    public interface IWeatherDataWriter
    {
        void WriteWeather(WeatherModel model);
    }

    public class WeatherDataWriter : IWeatherDataWriter
    {
        private readonly WorkerOptions _options;

        public WeatherDataWriter(WorkerOptions options)
        {
            _options = options;
        }
        public void WriteWeather(WeatherModel model)
        {
            var units = _options.Units;
            var temperature = _options.Units.ToUpper() == "C" ? model.current.temp_c : model.current.temp_f;
            var precitipation = model.current.precip_mm > 0 ? "True" : "False";

            if (!Directory.Exists(_options.CsvPath))
            {
                try
                {
                    Directory.CreateDirectory(_options.CsvPath);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Somthing wrong happend:", ex);
                }
            }

            var filename = Path.Combine(_options.CsvPath, _options.CsvFilename);
            var fileExists = File.Exists(filename);

            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename, true))
                {
                    if (!fileExists)
                        file.WriteLine("Date,Temperature,Unit,Precipitation");
                    file.WriteLine($"{DateTimeOffset.Now},{temperature},{units},{precitipation}");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Somthing wrong happend:", ex);
            }
        }
    }
}