using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using weatherservice;
using weatherservice.Services;

namespace weatherService
{
    public class Worker : BackgroundService
    {
        private readonly IWeatherApi _api;
        private readonly IWeatherDataWriter _writer;
        private readonly WorkerOptions _options;
        private readonly ILogger<Worker> _logger;

        public Worker(IWeatherApi api, IWeatherDataWriter writer, WorkerOptions options, ILogger<Worker> logger)
        {
            _api = api;
            _writer = writer;
            _options = options;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var timer = _options.Timer;
            
            while (!stoppingToken.IsCancellationRequested)
            {
                var model = _api.GetWeatherData();
                if (model != null)
                {
                    _logger.LogInformation("Getting current weather data for {city} at: {time}", model.location.name, DateTimeOffset.Now);
                    _writer.WriteWeather(model);
                }
                await Task.Delay(timer, stoppingToken);
            }
        }
    }
}
