using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging.EventLog;
using weatherservice;
using weatherservice.Services;

namespace weatherService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;
                    WorkerOptions options = configuration.GetSection("WCF").Get<WorkerOptions>();
                    services.AddSingleton(options);
                    
                    services.AddSingleton<IWeatherApi,WeatherApi>();
                    services.AddSingleton<IWeatherDataWriter,WeatherDataWriter>();

                    services.AddHostedService<Worker>()
                        .Configure<EventLogSettings>(config =>
                        {
                            config.LogName = "Weather Service";
                            config.SourceName = "Weather Service Source";
                        });

                }).UseWindowsService();
    }
}
