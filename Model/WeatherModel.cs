

using System.Collections.Generic;

namespace weatherservice.Model
{

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Location    {
        public string name { get; set; } 
        public string region { get; set; } 
        public string country { get; set; } 
        public double lat { get; set; } 
        public double lon { get; set; } 
        public string tz_id { get; set; } 
        public int localtime_epoch { get; set; } 
        public string localtime { get; set; } 
    }

    public class Condition    {
        public int code { get; set; } 
    }

    public class Current    {
        public int last_updated_epoch { get; set; } 
        public string last_updated { get; set; } 
        public double temp_c { get; set; } 
        public double temp_f { get; set; } 
        public Condition condition { get; set; } 
        public double precip_mm { get; set; } 
        public double precip_in { get; set; } 
        public int cloud { get; set; } 
        public double uv { get; set; } 
    }

    public class WeatherModel    {
        public Location location { get; set; } 
        public Current current { get; set; } 
    }


    
}