namespace weatherservice
{
    public class WorkerOptions
    {
        public string ApiKey { get; set; }
        public string City { get; set; }
        public string Units { get; set; }
        public int Timer { get; set; }
        public string CsvPath { get; set; }
        public string CsvFilename { get; set; }
    }
}